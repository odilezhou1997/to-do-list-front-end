
function showTime(){  
	let today = new Date();
	let year = today.getFullYear();
	let month = today.getMonth() + 1;
	let day = today.getDate();
	let dayToday = today.getDay();

	if (dayToday == 0) {
		dayToday = "Sun."
	}else if (dayToday == 1) {
		dayToday = "Mon."
	}else if (dayToday == 2) {
		dayToday = "Tue."
	}else if (dayToday == 3) {
		dayToday = "Wed."
	}else if (dayToday == 4) {
		dayToday = "Thu."
	}else if (dayToday == 5) {
		dayToday = "Fri. Finally!"
	}else if (dayToday == 6) {
		dayToday = "Sat."
    }
    
    let minute = today.getMinutes();
    if (minute < 10){
        minute = "0" + minute;
    }
	
    let second = today.getSeconds();
    if (second < 10){
        second = "0" + second;
    }
	
	let hour = today.getHours();
	let note = "Almost DDL!!!";

	if (hour < 10){
		time = "🌝" + "0" + hour + ":" + minute + ":" + second;
    } else if (hour >= 10 && hour <12) {
        time = "🌝" + hour + ":" + minute + ":" + second;
    }else if (hour === 12) {
        time = "🍵" + hour + ":" + minute + ":" + second;
    } else if (hour > 12 && hour < 18){
		hour = hour - 12;
		time = "🍵" + "0" + hour + ":" + minute + ":" + second;
	} else if (hour >= 18 && hour < 22){
        hour = hour - 12;
        time = "🌚" + "0" + hour + ":" + minute + ":" + second;
    } else if (hour >= 22) {
		hour = hour - 12;
        time = "🌚" + hour + ":" + minute + ":" + second + " " + note;
    }
    
    let nowTime = year + "."+ month + "." + day + " " + dayToday + " " + time + " " ; 
    document.getElementById("getTime").innerHTML = nowTime;

}

setInterval(function() {showTime()},1000);