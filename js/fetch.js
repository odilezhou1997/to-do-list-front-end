function getItems(currentStatus) {
    fetch('http://localhost:8004/items')
    .then(response => {
        return response.json();
    })
    .then(data => {
        itemStatus(currentStatus, data);
    });
}

function addItems(item){
    fetch('http://localhost:8004/items', {
        method:'POST', 
        body: JSON.stringify(item),
        headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8004/items',
            'Content-Type': 'application/json',
        }})
    .then((response) => {
        return response.json();
    })    
    .then(() => {
        getItems(currentStatus);
    });
}

function updateItem(item) {
    fetch ('http://localhost:8004/items', {
        method: 'PUT',
        headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8004/items',
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(item)
    })
    .then(response => {
        return response.json();
      })
    .then(() => {
        getItems(currentStatus);
    });
}

function deleteItem(id) {
    fetch('http://localhost:8004/items/' + id, {
        method: 'DELETE',
        headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8004/items',
            'Content-Type': 'application/json',
        }
    })
    .then(() => {
        getItems(currentStatus);
    });
}
