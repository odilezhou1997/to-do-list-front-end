// Need to be implement
let currentStatus = 'ALL';

// generate
function generateItem (item) {
    return "<li class=" + item.status 
    + "><input type='checkbox'" 
    + (item.status === "COMPLETED" ? "checked" : "") 
    + " onclick = 'changeStatus(" + item.id + ", \"" + item.text + "\", \"" + item.status + "\")'><span id='" 
    + item.id + "' class = 'none-outline' onclick = 'editable(this)' onkeydown = 'editItem(this, " + "\"" + item.status + "\"" + ")' onmouseout = 'uneditable(this)''>"
    + item.text + "</span><button onclick='deleteItems("
    + item.id + ")' class = 'delete-button'></button></li>";
}

// add
function addItem () {
    let inputValue = document.getElementById("input-item").value;
    if (inputValue.length === 0) {
            alert("Write Something, You Lazy!");
    } else {
        let item = {
            text: inputValue,
        };
        addItems(item);
        document.getElementById("input-item").value = "";
    }
}

// enter key
function enterItem (event) { 
    if (event.keyCode === 13) {
        addItem();
    }
}

// editable
function editItem(ele, status) {
    currentStatus = status;
    if (event.keyCode === 13) {
      const text = ele.innerText;
      const id = ele.getAttribute('id');
      let newItem = {
                id: id,
                text: text,
                status: currentStatus
            };
        updateItem(newItem);
    }
  }
    
function editable(ele) { 
    ele.contentEditable = true;

}

function uneditable(ele) {
    if (event.keyCode === 13) {
        ele.contentEditable = false;
    }
}

// filter
function filterItems (items) { 
    if (currentStatus === "ALL" && items !== undefined) {
        return items;
    } else if (currentStatus !== "ALL" && items !== undefined) {
        return items.filter(item => item.status === currentStatus);
    }
}

function generateItems (items) {
    return filterItems(items)
    .map(item => generateItem(item))
    .reduce((a, b) => a + b, "");
}

// itemStatus
function itemStatus (status, items) {
    currentStatus  = status;
    document.getElementById("things-to-do").innerHTML = generateItems(items);
}

function filterButton (dom, status) {
    let elements = dom.parentNode.children;
    Array.from(elements).forEach((ele) => ele.classList.remove('button-selected'));
    dom.classList.add('button-selected');
    getItems(status);
}

// change status 
function changeStatus(id, text, status) {
    let newItem = 
        {
            id: id,
            text: text,
            status: (status === "COMPLETED" ? "ACTIVE" : "COMPLETED")
        };
    updateItem(newItem);
  }

  
//delete
function deleteItems(id) {
    deleteItem(id);
}



